import cv2
import json
import yaml
import numpy as np
import pandas as pd
from itertools import combinations
import matplotlib.pyplot as plt
from lib.reference_points import ReferencePoints 

# TODO p-values etc.?
# TODO Figures?

def todo():
    df = pd.DataFrame()
    imagenames = [
        'DJI_0026', 'DJI_0029', 'DJI_0032', 'DJI_0035', 'DJI_0038', 
        'DJI_0045', 'DJI_0049', 'DJI_0053', 'DJI_0061', 'DJI_0066', 
        'DJI_0067', 'DJI_0078'
    ]
    # Load Top View for drawing
    fname_img_tv = '/Users/tobias/data/5Safe/vup/homography_evaluation/data/top_view/DJI_0017.JPG'
    img_tv = cv2.imread(fname_img_tv)
    img_tv = cv2.cvtColor(img_tv, cv2.COLOR_BGR2RGB)

    fig, axs = plt.subplots(4, 3)

    # Load every image and prepare dataframe for each image
    i = 0
    j = 0 
    for imagename in imagenames:
        fname_img_pv = f'/Users/tobias/data/5Safe/vup/homography_evaluation/data/perspective_views/{imagename}.JPG'
        # Load image for drawing
        img_pv = cv2.imread(fname_img_pv)
        img_pv = cv2.cvtColor(img_pv, cv2.COLOR_BGR2RGB)

        # Load Summary of experiment
        d = {}
        with open(f"out/{imagename}.JPG/summary.txt") as f:
            for line in f:
                (key, val) = line.split(';')
                d[key] = val

        distances_dict = json.loads(d['distances'].replace("\'", "\""))
        _df = pd.DataFrame([distances_dict])
        _df = _df.transpose()

        _df['image'] = imagename
        _df['point_quality'] = 'good'
        _df['angle'] = float(d['angle'])
        _df['altitude'] = float(d['altitude'])
        _df['n_ref_pts'] = int(d['nr_ref_pts'])
        _df = _df.reset_index()
        _df = _df.rename(columns={'index': 'pointname', 0: 'error'})

        # Load Reference points for Convex Hull to determine if val pt in ref pts hull
        ref_pts_pv = ReferencePoints.load(f'out/{imagename}.JPG/ref_pts_pv.txt')
        ref_pts = ref_pts_pv.get_numpy_arr()
        val_pts_pv = ReferencePoints.load(f'out/{imagename}.JPG/val_pts_pv.txt')
        val_pts = val_pts_pv.get_numpy_arr()
        hull = cv2.convexHull(ref_pts.astype(int).reshape(-1, 1, 2), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        axs[j, i].imshow(img_pv)
        hull = np.squeeze(hull)
        # For drawing need _hull
        _hull = np.concatenate((hull, [hull[0,:]]))
        axs[j, i].plot(_hull[:, 0], _hull[:, 1], color='white', lw=2.0)
        for name, val_pt in val_pts_pv.items():
            print(cv2.pointPolygonTest(hull, (val_pt.x, val_pt.y), True))
            color = 'blue'
            if cv2.pointPolygonTest(hull, (val_pt.x, val_pt.y), False) == -1:
                color = 'red'
                _df.loc[_df.pointname == name, 'point_quality'] = 'bad'
            axs[j, i].scatter(val_pt.x, val_pt.y, color=color)
        df = pd.concat([df, _df])
        axs[j, i].axis('off')
        i += 1
        if i % 3 == 0:
            i = 0
            j += 1
    plt.show()
    #df['error'].hist()
    #plt.show()

    # error to cm
    df.error = df.error*100

    # TODO: Before doing this, maybe I should only use 4 ref pts per image? To make them
    # better comparable.

    print('-'*10, 'Raw Dataframe', '-'*10)
    print(df)
    print('')

    print('-'*10, 'Error by Image and Point Quality', '-'*10)
    print(df.groupby(['image', 'point_quality'])['error'].mean())
    print('')

    print('-'*10, 'Error Point Quality', '-'*10)
    print(df.groupby(['point_quality'])['error'].mean())
    print('')

    print('-'*10, 'Error by Image', '-'*10)
    print(df.groupby(['image'])['error'].mean())
    print('')

    print('-'*10, 'Error by nr of RefPts', '-'*10)
    print(df.groupby(['n_ref_pts'])['error'].mean())
    print('')


    print('-'*10, 'Error by Altitude', '-'*10)
    print(df.groupby(['altitude'])['error'].mean())
    print('')

    print('-'*10, 'Error by Altitude and Point Quality', '-'*10)
    print(df.groupby(['altitude', 'point_quality'])['error'].mean())
    print('')


    print('-'*10, 'Error by Angle', '-'*10)
    print(df.groupby(['angle'])['error'].mean())
    print('')

    df.groupby(['angle'])['error'].mean().plot()
    plt.show()
    #df.groupby(['angle'])['error'].plot(kind='scatter')
    #plt.show()


    df.boxplot(by='angle', column='error')
    df[df.point_quality == 'good'].boxplot(by='angle', column='error')
    #df.plot(kind='scatter', x='angle', y='error')
    plt.show()



def reference_points_experiment(img_name):
    # Load Config
    with open(f'conf/config_{img_name}.yaml', 'r') as file:
        cfg = yaml.safe_load(file)

    fname_val_pts_pv = cfg['perspective_view']['fname_val_pts']
    fname_ref_pts_pv = cfg['perspective_view']['fname_ref_pts']

    fname_val_pts_tv = cfg['top_view']['fname_val_pts']
    fname_ref_pts_tv = cfg['top_view']['fname_ref_pts']
    scaling_factor = cfg['top_view']['scaling_factor']

    selection_ref_pts = cfg['selection']['ref_pts']
    selection_val_pts = cfg['selection']['val_pts']

    val_pts_pv = ReferencePoints.load(fname_val_pts_pv)[selection_val_pts]
    val_pts_tv = ReferencePoints.load(fname_val_pts_tv)[selection_val_pts]

    data = []

    # For every combination
    for comb_len in range(4, len(selection_ref_pts)+1):
        print(f'start with {comb_len} nr of ref pts')
        combs = list(combinations(selection_ref_pts, comb_len))
        print(f'!!nr of experiments: {len(combs)}!!')

        # For every Experiment
        for i, comb in enumerate(combs):
            print(f'exp: {comb_len}, {i}')
            ref_pts_pv = ReferencePoints.load(fname_ref_pts_pv)[comb]
            ref_pts_tv = ReferencePoints.load(fname_ref_pts_tv)[comb]

            h = ReferencePoints.calc_homography_matrix(
                ref_pts_pv,
                ref_pts_tv
            )
            val_pts_pv_transformed = val_pts_pv.transform(h)
            distances, mean_d, var_d, std_d, nr_pts = ReferencePoints.calc_distances(
                val_pts_pv_transformed,
                val_pts_tv,
                scaling_factor
            )

            hull = cv2.convexHull(ref_pts_pv.get_numpy_arr().astype(int).reshape(-1, 1, 2), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            hull = np.squeeze(hull)
            _hull = np.concatenate((hull, [hull[0,:]]))

            for name, val_pt in val_pts_pv.items():
                quality = get_validation_point_quality(val_pt, hull)
                data.append({
                    'img': img_name,
                    'comb': comb_len,
                    'exp': i,
                    'val_pt': name,
                    'val_pt_x': val_pt.x,
                    'val_pt_y': val_pt.y,
                    'quality': quality,
                    'hull': hull,
                    'hull_len': len(hull),
                    'dist_to_hull': cv2.pointPolygonTest(_hull, (val_pt.x, val_pt.y), True),
                    'hull_area': cv2.contourArea(_hull),
                    'ref_pts': ref_pts_pv.get_numpy_arr(),
                    'n_ref_pts': len(ref_pts_pv),
                    'error': distances[name]*100
                })
    df = pd.DataFrame(data)
    return df


def get_validation_point_quality(val_pt, hull):
    if cv2.pointPolygonTest(hull, (val_pt.x, val_pt.y), False) == -1:
        return 'bad'
    return 'good'


def create_data(fname):
    df = reference_points_experiment(fname)
    df.to_pickle(f'out/statistics/DJI_0026_df.pickle')


def read_data(fname):
    df = pd.read_pickle(f'out/statistics/{fname}_df.pickle')
    return df


def filter_bad_hulls(df):
    return df[df.hull_len > 3]

def filter_bad_points(df):
    return df[df.quality == 'good']


def get_n_worst_experiments(df, n):
    groups = df.groupby(['comb', 'exp']).agg(mean_error=('error', 'mean')).sort_values(by='mean_error').reset_index()
    return groups.iloc[-n:]


def plot_experiment(df, comb, exp):
    exp_df = df[(df.comb == comb) & (df.exp == exp)]
    mean_error = exp_df.error.mean()
    fname = f'/Users/tobias/ziegleto/data/5Safe/vup/homography_evaluation/data/perspective_views/{exp_df.img.values[0]}.JPG'
    img = cv2.imread(fname)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Plotting
    plt.imshow(img)

    # Plot Reference Points
    ref_pts = exp_df.ref_pts.values[0]
    plt.scatter(ref_pts[:, 0], ref_pts[:, 1], color='white', alpha=0.4, s=80)

    # Plot Hull
    hull = exp_df.hull.values[0]
    plot_hull(hull)

    # Plot Validation Points
    plot_validation_points(exp_df)
    plt.text(0, 0, f'error: {mean_error:.2f}')

    plt.show()


def plot_validation_points(exp_df):
    for idx, row in exp_df.iterrows():
        color = 'green' if row.quality == 'good' else 'red'
        plt.scatter(row.val_pt_x, row.val_pt_y, c=color, s=80, alpha=0.6)
        plt.text(row.val_pt_x, row.val_pt_y, f'E:{row.error:.2f}cm')


def plot_hull(hull):
    _hull = np.concatenate((hull, [hull[0,:]]))
    plt.plot(_hull[:, 0], _hull[:, 1], color='white', lw=2.0)


def get_best_of_each_exp(df):
    groups = df.groupby(['comb', 'exp']).agg(mean_error=('error', 'mean')).reset_index()
    groups = groups.sort_values(by='mean_error')
    bestest = {}
    for i in range(4, 10):
        best_exp = groups[groups.comb==i].iloc[0]
        bestest[int(best_exp['comb'])] = {}
        bestest[int(best_exp['comb'])]['exp'] = int(best_exp['exp'])
        bestest[int(best_exp['comb'])]['mean_error'] = best_exp['mean_error']
    #groups = groups.groupby('comb', as_index=False).agg(mean_error=('mean_error', 'min'))
    return bestest

def get_overall_result(df):
    groups = df.groupby(['comb']).agg(mean_error=('error', 'mean'))
    print(groups)

def get_result_after_quality(df):
    groups = df.groupby(['comb', 'quality']).agg(mean_error=('error', 'mean'))
    print(groups)

if __name__ == '__main__':
    pd.set_option("display.precision", 2)
    #create_data('DJI_0026')
    df = read_data('DJI_0026')
    #plot_experiment(df, 5, 15)
    df = filter_bad_hulls(df) 

    #get_overall_result(df)

    #get_result_after_quality(df) 

    #df = filter_bad_points(df)

    #df = df.groupby(['comb', 'exp']).agg(mean_error=('error', 'mean'))
    #df = df.reset_index()
    #print(df)
    #df = df[df.mean_error <= 6.95]
    #plt.scatter(df.comb, df.mean_error)
    #plt.hlines(6.95, 4, 9, 'black')
    #plt.show()

    # 1. get the expeirments that are overall < 6.95
    # 2. select them from new dataframe
    df_ = df.groupby(['comb', 'exp']).agg(mean_error=('error', 'mean'))
    df_ = df_.reset_index()
    df_ = df_[df_.mean_error <= 6.95]
    exps = df_['exp'].values.tolist()
    comps = df_['comb'].values.tolist()
    #print(exps.values)
    #df = df[df['exp'].isin(exps)]
    # comb + exp...
    df = df.set_index(['exp', 'comb']).loc[zip(exps, comps)].reset_index()
    print(df)
    df = df.groupby(['comb', 'exp', 'quality', 'val_pt']).agg(mean_error=('error', 'mean'))
    df = df.reset_index()
    #df['color'] = 'green' if df['quality'] == 'good' else 'red'
    df['color'] = np.where(df['quality'] == 'good', 'green', 'red')
    plt.scatter(df.comb, df.mean_error, c=df.color)
    plt.hlines(6.95, 4, 9, 'black')
    plt.show()
   # print(df[df.comb==9])


    #losers = get_n_worst_experiments(df, 5)
    #print(losers)
    # NA SCHAU MAL EINER AN: wenn man nur noch good points nimmt, dann error maximal 25cm.
    # Und die schlechtestens fuenf sind dann die, die so ne komische spitze haben.

    # EXPERIMENT OVERALL ------
    #get_overall_result(df) # Klar je mehr punkte desto besser, ABER
    #df = filter_bad_points(df)
    #get_overall_result(df) # Abstand geringer. 6-9 minimal
    #df = filter_bad_hulls(df)
    #get_overall_result(df) # 4 Holt auf.
    # -----------------

    # EXPERIMENT WORST OF EACH ----
    #losers = get_n_worst_experiments(df, 5) # FOUR, but...
    #print(losers)
    #df = filter_bad_points(df)
    #df = filter_bad_hulls(df)
    #losers = get_n_worst_experiments(df, 15) 
    #plot_experiment(df, 4, 11)
    #print(losers) # 4 - 5
    # --- TODO

    # EXPERIMENT BEST OF EACH --------
    #best_of_each_exp = get_best_of_each_exp(df)
    #for comb, exp in best_of_each_exp.items():
    #    print(f'{comb}, {exp["mean_error"]:.2f}')
    #    plt.scatter(comb, exp['mean_error'])
        #plot_experiment(df, comb, exp['exp'])
    #plt.show()


    # Die Besten Ergebnisse haben wir zwar mit vier Punkten, aber die Varianz ist enorm bei vier Punkten!!!
    # Auch die schlechtesten Ergebnisse natuerlich bei vier punkten. auch wenn wir nur good points und  ..., einbeziehen.
    # TODO Nur wenn len hull == len ref pts?
    # ------


